import { createStore, applyMiddleware } from 'redux';
import { syncHistoryWithStore } from 'react-router-redux';
import { browserHistory } from 'react-router';
import { persistStore } from 'redux-persist';

import createSagaMiddleware from 'redux-saga';
import rootSaga from './sagas/index';

import rootReducer from './reducers/index';

// import sagaMonitor from './monitor';
const sagaMiddleware = createSagaMiddleware();
export const store = createStore(rootReducer, undefined, applyMiddleware(sagaMiddleware));
sagaMiddleware.run(rootSaga);


export const persistor = persistStore(store);
export const history = syncHistoryWithStore(browserHistory, store);

