import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './internal/serviceWorker';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react'

import Routes from './routes';
import { store, persistor } from './store';

import './assets/css/bootstrap.min.css';
import './assets/scss/now-ui-kit.scss';
import './assets/demo/demo.css';
import './assets/demo/nucleo-icons-page-styles.css';

const app = (
   <Provider store={store}>
      <PersistGate persistor={persistor}>
         <Routes />
      </PersistGate>
   </Provider>
);

ReactDOM.render(
   app,
   document.getElementById('root'),
);

serviceWorker.unregister();

// todo: add loader when requesting
// todo: add middleware for authenticated and unauthenticated pages
// todo: explore how to deploy
// todo: search for nucleo font free
