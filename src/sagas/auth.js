import axios from "axios";
import { put } from 'redux-saga/effects';

axios.defaults.baseURL = process.env.REACT_APP_API_URL;
axios.defaults.headers.common = {
   'Accept': 'application/json, application/xml, text/play, text/html, *.*',
   'Content-Type': 'application/json',
};

export function * loginAsync(params) {
   const { email, password } = params.payload;

   try {
      const url = '/auth/login';

      // todo: improve authorization; add to env; make it buffer
      const res = yield axios.post(url, {
         email: email, password: password
      }, { headers: { 'Authorization': 'Basic Y2xpZW50X2lkOmNsaWVudF9zZWNyZXQ=' }});

      // todo: store locally auth data
      const data = res.data;
      yield put({ type: 'LOGIN', payload: data });
   } catch (e) {
      // todo: capture error
      console.log('e', e);
   }
}

export function * registerAsync(params) {
   const { email, password, firstName, lastName, } = params.payload;

   try {
      const url = '/auth/register';

      // todo: improve authorization; add to env; make it buffer
      const res = yield axios.post(url, {
         email, password, firstName, lastName
      }, { headers: { 'Authorization': 'Basic Y2xpZW50X2lkOmNsaWVudF9zZWNyZXQ=' }});

      // todo: store locally auth data
      const data = res.data;
      yield put({ type: 'REGISTER', payload: data });
   } catch (e) {
      // todo: capture error
      console.log('e', e);
   }
}
