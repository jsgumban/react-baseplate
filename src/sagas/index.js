import { takeLatest } from 'redux-saga/effects';
import { loginAsync, registerAsync } from './auth';

export default function* rootSaga() {
  yield takeLatest('LOGIN_ASYNC', loginAsync);
  yield takeLatest('REGISTER_ASYNC', registerAsync);
}
