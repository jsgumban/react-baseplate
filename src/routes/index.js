import React from 'react';
import { Router, Route, IndexRoute } from 'react-router';

import { history } from '../store';
import Login from "./login";
import Register from "./register";

export default () => {
   return (
      <Router history={history}>
         <Route path="/">
            <IndexRoute exact component={Login} />
         </Route>
         <Route path="/register">
            <IndexRoute exact component={Register} />
         </Route>
      </Router>
  );
}

// todo: learn sub-routes


