import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from "react-redux";
import './style.scss';

import {
   Button,
   Card,
   CardBody,
   CardFooter,
   Form,
   Input,
   InputGroupAddon,
   InputGroupText,
   InputGroup,
   Container,
   Col
} from 'reactstrap';


const Login = () => {
   const props = useSelector(props => props);
   const dispatch = useDispatch();

   const [firstFocus, setFirstFocus] = useState(false);
   const [lastFocus, setLastFocus] = useState(false);

   const [email, setEmail] = useState(null);
   const [password, setPassword] = useState(null);

   const handleFormUpdate = (event) => {
      const name = event.target.name;
      const value = event.target.value;

      switch (name) {
         case 'email':
            setEmail(value);
            break;
         case 'password':
            setPassword(value);
            break;
      }
   };

   const handleFormSubmit = () => {
      dispatch({
         type: 'LOGIN_ASYNC',
         payload: {
            email,
            password,
         }
      });
   };

   useEffect(() => {
      // todo: redirect to home when authenticated
      console.log('props.auth', props.auth);
   });

   return (
      <div className='page-header clear-filter'>
         <div className='content'>
            <Container>
               <Col className='ml-auto mr-auto' md='4'>
                  <Card className='card-login card-plain'>
                     <Form action='' className='form' method=''>
                        <CardBody>
                           <InputGroup className={'no-border input-lg' + (firstFocus ? ' input-group-focus' : '')}>
                              <InputGroupAddon addonType='prepend'>
                                 <InputGroupText>
                                    <i className='now-ui-icons ui-1_email-85'></i>
                                 </InputGroupText>
                              </InputGroupAddon>
                              <Input
                                 placeholder='Email'
                                 type='text'
                                 onFocus={() => setFirstFocus(true)}
                                 onBlur={() => setFirstFocus(false)}
                                 name='email'
                                 onChange={handleFormUpdate}>
                              </Input>
                           </InputGroup>
                           <InputGroup className={'no-border input-lg' + (lastFocus ? ' input-group-focus' : '')}>
                              <InputGroupAddon addonType='prepend'>
                                 <InputGroupText>
                                    <i className='now-ui-icons objects_key-25'></i>
                                 </InputGroupText>
                              </InputGroupAddon>
                              <Input
                                 placeholder='Password'
                                 type='password'
                                 onFocus={() => setLastFocus(true)}
                                 onBlur={() => setLastFocus(false)}
                                 name='password'
                                 onChange={handleFormUpdate}>
                              </Input>
                           </InputGroup>
                        </CardBody>
                        <CardFooter className='text-center'>
                           <Button
                              block
                              className='btn-round'
                              color='info'
                              onClick={handleFormSubmit}
                              size='lg'>
                              LOGIN
                           </Button>
                        </CardFooter>
                     </Form>
                  </Card>
               </Col>
            </Container>
         </div>
      </div>
   );
};


export default Login;
// todo: add redirect to register
