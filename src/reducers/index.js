import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import auth from './auth';

const persistConfig = {
   key: 'root',
   storage,
   debug: true,
   whitelist: ['auth']
};

const rootReducer = combineReducers({
   routing: routerReducer,
   auth
});

export default persistReducer(persistConfig, rootReducer);
