import { persistReducer } from 'redux-persist';
import storage from "redux-persist/es/storage";

const initialState = {
   currentUser: null
};

const auth = (state = initialState, action) => {
   const newState = { ...state };
   switch (action.type) {
      case 'LOGIN' || 'REGISTER':
         newState.currentUser = action.payload.data;
         break;
   }
   return newState;

};

export default persistReducer({
   key: 'auth',
   storage,
}, auth);
